import random

type
  Sides* = uint


converter toSides*(val: int): Sides {.raises: ValueError.} =
  ## Converts an int value into a Sides type. It will raise an error if the value is less than 2.
  if val < 2: raise newException(ValueError, "A die cannont have " & $val & " sides.")
  result = (Sides)val

converter toSides*(val: uint): Sides {.raises: ValueError.} =
  ## Converts a unit value into a Sides type. It will raise an error if the value is less than 2.
  toSides((int)val)

converter toInt(sides: Sides): int = (int)sides

proc roll*(sides: Sides): int =
  ## Roll a dice with the number of dice provided.
  result = rand(1..sides.toInt())

proc roll*(sides: Sides, times: int): seq[int] =
  ## Roll a dice the given number of times.
  # Make sure times is at least 1
  if times < 1: raise newException(ValueError,
      "Times should be greater than 1 but was " & $times)

  # Check for a one time roll
  if times == 1:
    return @[roll(sides)]

  # Create a new sequence for the results
  result = newSeq[int]()

  # Loop the requested number of times
  for i in 0..(times - 1):
    # Calculate the random value and add the the sequence
    result.add(roll(sides))

proc sum*(rolls: seq[int]): int =
  ## Sum all the rolls

  # Loop through the rolls
  for roll in rolls:
    result += roll

when isMainModule:
  import flag
  import strformat

  # Initialize the random
  randomize()

  const
    version = "0.1.3"
    usage = """Usage: nimdice [options]

Command line dice rolling simulator.

Options:
   --help, -h             Displays this help message.
   --version, -V          Displays the version.
   --sides, -s [number]   Sets the number of sides. Should be greater than 2.
   --times, -c [number]   The number of times to role the dice. Should be at least 1.
   --total, -t            Total the rolls. Ingored if only 1 roll is requested.
   --average, -a          Average all the roles. Ignored if only 1 roll is requested."""

  var
    sides = 6
    times = 1
    total = false
    average = false
    showHelp = false
    showVersion = false

  # Create a new flag set
  let fs = newFlagSet()

  # Add the flags
  fs.add(sides.addr, "s", "sides",
    "Sets the number of sides. Should be greater than 2.")
  fs.add(times.addr, "c", "times",
    "The number of times to role the dice. Should be at least 1.")
  fs.add(total.addr, "t", "total",
    "Total the rolls. Ignored if only 1 roll is requested.")
  fs.add(average.addr, "a", "average",
    "Averate all the roles. Ignored if only 1 roll is requested.")
  fs.add(showHelp.addr, "help", "Displays this help message.")
  fs.add(showVersion.addr, "V", "version", "Displays the version.")

  # Parse the flags
  fs.parse()

  # Check if the version should be shown
  if showVersion:
    quit(version)

  # Check if the usage should be shown
  if showHelp:
    quit(usage)

  # Check how many times the user requested to roll the dice
  if times == 1:
    # No other option is valid. Just roll and exit
    echo fmt"Rolled a {sides} sided die and got {sides.roll}"
  elif times > 1:
    # Roll the specified number of times
    let rolls = roll(sides, times)

    # Display the rols
    echo fmt"Rolled a {sides} sided die {times} time(s) and got {rolls}"

    # Check if we should total or sum the rolls
    if total or average:
      let totalRolls = rolls.sum()

      if total:
        echo fmt"Total: {totalRolls}"
      if average:
        echo fmt"Average: {totalRolls / times}"
  else:
    quit("Error: Times should at least be one\n", 1)
