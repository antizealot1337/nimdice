import unittest, nimdice

proc `==`(side: Sides, other: int): bool = ((int)side) == other

suite "Testing toSides":
        test "int > 1 toSides":
                let two = 2.toSides()
                check(two == 2)

        test "int <=1 toSides":
                let badVals = @[-20, -1, 0, 1]
                for val in badVals:
                        try:
                                discard (val).toSides()
                                fail() # A success of the above statement is a failure
                        except: discard

        test "uint > 1 toSides":
                let
                        twoUint: uint = 2
                        two = twoUint.toSides()
                check(two == 2)

        test "uint <= 1 toSides":
                let badVals = @[0, 1]
                for val in badVals:
                        try:
                                discard ((uint) val).toSides()
                                fail() # A success of the above statement is a failure
                        except: discard

suite "Testing roll(Sides)":
        test "roll results":
                let sides = 6.toSides()
                for i in 0..10:
                        let r = roll(sides)
                        check(r > 0)
                        check(r < 7)

suite "Testing roll(Sides, int)":
        test "bad number of rolls":
                let badVals = @[-20, -1, 0]
                for val in badVals:
                      try:
                                discard roll(2.toSides(), -1)
                                fail() # A success of the above statement is a failure
                      except: discard

        test "multiple rolls":
                let
                        sides = 6.toSides()
                        numRolls = 6
                        rolls = roll(sides, numRolls)

                check(rolls.len() == numRolls)

suite "Testing sum(seq[int])":
        test "summing a sequence of ints":
                let rolls = @[1, 1, 1, 1, 1, 1]
                check(rolls.sum() == 6)
