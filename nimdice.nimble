version       = "0.1.3"
author        = "anitzealot1337"
description   = "A library and command line program for simulating rolling dice"
license       = "MIT"
srcDir        = "src"
bin           = @["nimdice"]

requires: "nim >= 1.0"
requires: "https://gitlab.com/antizealot1337/flag.git#v0.7.0"
